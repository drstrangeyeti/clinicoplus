\documentclass[a4paper, 11pt]{article}
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{textcomp}
\usepackage{longtable}

\usepackage{tikz}
\usetikzlibrary{shadows}

% Função para desenhar teclas
\newcommand*\keystroke[1]{%
  \tikz[baseline=(key.base)]
    \node[%
      draw,
      fill=white,
      drop shadow={shadow xshift=0.25ex,shadow yshift=-0.25ex,fill=black,opacity=0.75},
      rectangle,
      rounded corners=2pt,
      inner sep=1.5pt,
      line width=0.5pt,
      font=\scriptsize\sffamily
    ](key) {#1\strut}
  ;
}


\title{ClínicoPlus}
\author{João Rodrigo Ferreira}

\begin{document}
    \maketitle

    \tableofcontents
    \newpage

    \section{Introdução}

    \subsection{O que é o ClínicoPlus?}

    O SClínico tem vindo a tornar-se na plataforma única de registo clínico nas consultas de 
    Medicina Geral e Familiar.
    
    Apesar de constantes melhorias, a aplicação carece de algumas funcionalidades que podem 
    ser consideradas vitais para \emph{utilizadores avançados}. É o caso de combinações de 
    teclas de atalho (tal como \keystroke{Ctrl} + \keystroke{S} para gravar um documento).

    Os utilizadores do MedicineOne® tinham, relativos às suas Unidades, protocolos de análises 
    específicos, que não foram migrados para o sistema SClínico. 
    Tal deveu-se, segundo a informação obtida, à incapacidade deste sistema 
    diferenciar os protocolos por Unidade, sendo que estes estariam disponíveis para 
    todos as USF e UCSP que partilham o mesmo servidor. Este facto agrava-se com a 
    questionável adequação de alguns protocolos de análises disponíveis no SClínico. 
    A escolha manual de análises, uma a uma, além de morosa, é sujeita a potenciais erros e omissões.

    Os actos praticados pelo Médico de Família são individuais e personalizados. No entanto, 
    tal não significa que não existe muita repetibilidade nos actos de registo. 
    Enquanto uma auscultação cardio-pulmonar é diferente das outras que foram feitas por um Médico 
    durante a sua vida, o registo de uma auscultação normal é relativamente similar. O mesmo se passa 
    com outros actos que são diariamente registados. A opção frequentemente tomada é a redução da 
    verbosidade (e assim do tempo de escrita), utilizando siglas, que frequentemente não são 
    universais ou podem não ser correctamente interpretadas pelo profissional de outro serviço 
    ou um interno com menos experiência.

    Os Módulos do ClínicoPlus pretendem atenuar estas lacunas na prática do dia-a-dia, 
    para melhorar a qualidade de vida (e de registo) dos Médicos que o utilizam, diminuir 
    os erros, e permitir que todos possamos prestar mais tempo a escutar e tratar o utente, 
    e menos a verificar e corrigir erros, a navegar entre ecrãs e clicar em botões.
    
    \vspace*{\fill}

    
    \begin{quote}
        ``Não há que ser forte. Há que ser flexível.''
        \begin{flushright}
            --- Provérbio Chinês
        \end{flushright}
    \end{quote}
    
    
    \newpage
    \subsection{O que é e como foi feito o ClínicoPlus?}

    O ClínicoPlus é um conjunto de \emph{scripts} escritos para funcionarem com 
    o \mbox{\emph{AutoHotKey}}, da \emph{AutoHotkey Foundation LLC}, disponível gratuitamente.


    \texttt{https://www.autohotkey.com/}

    Poderá fazer o download do Autohotkey e substituir o executável presente na pasta ClínicoPlus, sem quaisquer problemas.

    \subsection{Versão e história}

    Esta versão tem o número 0.3.
    \subsubsection{Histórico de Versões}
    \begin{itemize}
        \item \textbf{0.1}: Módulo de Análises como executável \emph{standalone}.
        \item \textbf{0.2}: Adicionados os módulos de Atalhos e Texto Rápido, primeira versão com Manual.
            \begin{quote}                
                Curiosidade: O primeiro módulo criado foi na realidade o de Texto Rápido, em 2015, para o MedicineOne®
            \end{quote}
        \item \textbf{0.3}: Adicionado o módulo PEM Auto, o código foi reorganizado.
    \end{itemize}

    \subsection{Autoria e contribuições}

    A \emph{suite} foi escrita e preparada por João Rodrigo Ferreira.

    O módulo \emph{PEM Auto} foi baseado na ideia do Dr. João Paulo Alves, com o código original escrito pelo mesmo e adaptado pelo autor principal.

    \newpage
    \section{Instruções}

    \subsection{Instalação}

    A instalação dos módulos ClínicoPlus é simples.\\

    Ao transferir o pacote \emph{zip} do site \verb|https://notadoctor.me|, descomprima-o 
    com qualquer ferramenta, para uma pasta à sua escolha.

    \subsection{Iniciar a aplicação}

    A aplicação pode ser iniciada, executando o ficheiro \verb|clinico_plus| (poderá ser mostrado como \verb|clinico_plus.bat|, caso o seu sistema operativo tenha as extensões de ficheiro visíveis).

    Ao ser executado, verá aparecer momentaneamente uma janela preta (linha de comandos), e seguidamente aparecerá na barra do menu iniciar, no canto inferior direito, um símbolo \texttt{H}, sob fundo verde.

    \textbf{Quando desliga o computador, a aplicação é encerrada, pelo que terá que a iniciar cada vez que o liga, para a utilizar.}

    A aplicação é iniciada com os três módulos actuais activos:
    \begin{itemize}
        \item Análises
        \item Texto Rápido
        \item Atalhos
    \end{itemize}

    Cada um destes módulos é descrito na secção apropriada, abaixo.

    Na secção \ref{config}, página \pageref{config}, pode consultar informações sobre como desligar, configurar e extender os vários módulos existentes.






    \subsection{Módulo Análises}

    O \emph{módulo Análises} expõe a funcionalidade de inserir automáticamente perfis analíticos pré-configurados. O processo é automático, e embora seja mais lento do que a escolha de um protocolo padrão do SClínico, demorando 1~a~10~segundos conforme a dimensão do perfil, acrescenta perfis que poderão ser úteis à prática clínica.

    \subsubsection{Utilização}

    \begin{enumerate}
        \item No \texttt{SClínico}, navegue ao ecrã de MCDT, onde são geralmente pedidos os exames;
        \item Active o cursor na caixa de texto onde habitualmente escreve o nome do exame que deseja pedir;
        \item Escreva o nome do perfil e carregue na tecla \keystroke{Enter} ou \keystroke{Espaço};
        \begin{itemize}
            \item Exemplo: \texttt{>basico} \keystroke{Enter}
        \end{itemize}
        
        \begin{center}
            \textbf{Nota Importante:} Não se esqueça de adicionar o símbolo \texttt{>} no início do nome do perfil! Este é essencial para que o texto automático seja executado!
        \end{center}
        \item Aguarde enquanto os exames são adicionados à lista de pedidos.
    \end{enumerate}

    \subsubsection{Perfis disponíveis}

    Estão disponíveis actualmente os perfis enumerados na tabela \ref{table:exams}:

    \begin{table}[hb]
        \caption{Perfis de exames disponíveis}
        \label{table:exams}
        \begin{tabular}{l p{9cm}}
            \hline
            \textbf{Perfil} & \textbf{Exames inseridos}\\
            \hline
            >basico & hemograma com plaquetas, glucose, ionograma, tsh, 338, AST, ALT, gamaglutamil transferase, fosfatase alcalina, 1029, 412, 620, 625, creatinina, 627\\
            >alargado & 1313, 1318, proteínas (total) e electroforese, ionograma, 390, 499, urocultura, 1314, 1017, 1302, INR\\
            \hline
            >lipidos & CK, Colesterol Total, Colesterol HDL, Triglicéridos\\
            \hline
            >hta & ECG, hemograma com plaquetas, glucose, 338, 1318, 1029, 412, 620, ionograma, 625, creatinina, 627\\
            \hline
            >htasec & 1053, FT4, ??(VMA), 690, 1009, 1010, 663, 1032, 1315, ionograma, renina\\
            \hline
            >anemia & hemograma com plaquetas, 484, 483, capacidade de fixação de ferro, 329, cianocobalamina\\
            \hline
            >diabetes & hemograma com plaquetas, A1c, glucose, 1318, 1029, 412, 620, creatinina, 627, urocultura\\
            \hline
        \end{tabular}
    \end{table}






    \subsection{Módulo de Texto Rápido}

    O \emph{módulo Análises} permite substituir pequenas siglas pré-configuradas em textos descritivos de maiores dimensões.

    \subsubsection{Utilização}

    Para utilizar, basta escrever no texto do SOAP a abreviatura que escolheu (apresentadas na tabela)

    Exemplificando, ao escrever \texttt{acpn} \keystroke{Espaço} (abreviação para ``auscultação cardio-pulmonar normal''), o texto é automáticamente substituído por:\\        
        \texttt{AC: S1+S2, rítmicos, não ausculto sopros ou extrassons\\
        AP: MV mantido, sem RA}

    \subsubsection{Textos rápidos disponíveis}

    As substituições disponíveis são apresentadas na tabela \ref{table:textosrapidos}.

    \begin{longtable}{l p{9cm}}
        \caption{Textos rápidos}\\
        \label{table:textosrapidos}
        \textbf{Abreviatura} & \textbf{Substituição}\\
        \hline
        hta.s & Vem para consulta de manutenção da saúde - HTA.\\
        \hline
        hta.p & Peço controlo analítico, incluíndo microalbuminúria e perfil lipídico.\newline
                Peço registo de TA em ambulatório.\newline
                Dou recomendações de dieta e exercício físico.\\
        \hline
        dm.s &  Vem para consulta de manutenção da saúde - DM.\\
        \hline
        dm.p &  Peço controlo analítico, incluíndo perfil lipídico, microalbuminúria.\newline
                Peço HbA1c.\newline
                Dou recomendações de dieta e exercício físico.\\
        \hline
        is.p & Recomendações gerais.\newline Explico sinais de alarme que devem motivar reobservação.\\
        \hline
        si.o & Boa vitalidade.\newline
                Não palpo adenopatias.\newline
                Orofaringe: sem alterações\newline
                Otoscopia:\newline
                    OD: sem alterações\newline
                    OE: sem alterações\newline
                AC: s1+s2, não ausculto sopros\newline
                AP: MV mantido, não ausculto RA\newline
                Abdómen:\newline
                    mole, depressível, indolor à palpação. \newline
                    Não palpo massas ou organomegalias. \newline
                    Sem alterações à percussão RHA+\newline
                Sem edemas periféricos.\\
        \hline
        sa.o & Bom estado geral.\newline
                Pele e mucosas coradas e hidratadas.\newline
                Eupneico\newline
                AC: S1+S2, rítmicos, não ausculto sopros ou extrassons\newline
                AP: MV mantido, sem RA\newline
                Sem edemas periféricos\\
        \hline
        abdn & Abdómen mole, depressível, indolor à palpação, não palpo massas ou organomegalias. Sem alterações à percussão. RHA+\\
        \hline
        acpn & AC: S1+S2, rítmicos, não ausculto sopros ou extrassons\newline AP: MV mantido, sem RA\\
        \hline
        .a98 & Vem para consulta de manutenção da saúde\\
        \hline
        .a45 & Recomendações gerais.\\
        \hline
        .beg & Bom estado geral.\\
        \hline
        .a60 ou exames.s & Vem mostrar exames pedidos na consulta anterior.\\
        \hline
    \end{longtable}





    \subsection{Módulo de Atalhos}

    O \emph{módulo de Atalhos} permite aceder aos vários botões do SClínico através de combinações de teclas, possibilitando assim uma navegação mais fluída para os utilizadores que preferem usar o teclado. 
    
    A utilização dos atalhos facilita ainda aceder a botões que estão por detrás de secções diferentes do ecrã do \texttt{SOAP}, como é o caso dos programas de saúde, que se encontram dentro da secção \texttt{O} do \texttt{SOAP}.

    \subsubsection{Utilização}

    Pressione a combinação de teclas para executar a função pretendida.

    Por exemplo, para entrar no ecrã de \texttt{Resultados de MCDT}, enquanto está no ecrã de \texttt{SOAP}, poderá carregar na combinação \keystroke{Ctrl}+ \keystroke{R}.

    \subsubsection{Combinações disponíveis}

    Os atalhos disponíveis variam consoante o ecrã em que o SClínico se encontra. Podem ser consultados na tabela \ref{table:atalhos}.

    \begin{longtable}{l p{9cm}}
        \caption{Combinações de teclas}\\
        \label{table:atalhos}\\
        \hline
        \multicolumn{2}{c}{\textbf{Geral}}\\
        \hline
        \textbf{Combinação} & \textbf{Acção Executada}\\
        \hline
        \keystroke{Esc} & Sair do ecrã actual\\
        \keystroke{Ctrl}+ \keystroke{S} & Guardar\\
        \keystroke{Ctrl}+ \keystroke{P} & Imprimir\\
        \hline
        \hline
        \multicolumn{2}{c}{\textbf{Ecrã \texttt{Agenda}}}\\
        \hline
        \textbf{Combinação} & \textbf{Acção Executada}\\
        \hline
        \keystroke{Ctrl}+ \keystroke{R} & Actualizar agenda (ver novas confirmações de consultas)\\
        \hline
        \hline
        \multicolumn{2}{c}{\textbf{Ecrã \texttt{SOAP}}}\\
        \hline
        \textbf{Combinação} & \textbf{Acção Executada}\\
        \hline
        \keystroke{Ctrl}+ \keystroke{R} & Registar resultados de exames\\
        \keystroke{Ctrl}+ \keystroke{P} & Abrir a PEM\\
        \keystroke{Ctrl}+ \keystroke{A} & Pedidos de MCDT\\
        \keystroke{Ctrl}+ \keystroke{M} & Marcar consulta\\
        \keystroke{Ctrl}+ \keystroke{1} & Secção \texttt{S}\\
        \keystroke{Ctrl}+ \keystroke{2} & Secção \texttt{O}\\
        \keystroke{Ctrl}+ \keystroke{3} & Secção \texttt{A}\\
        \keystroke{Ctrl}+ \keystroke{4} & Secção \texttt{P}\\
        \keystroke{Ctrl}+ \keystroke{0} & Secção \texttt{Episódios Activos}\\
        \keystroke{Ctrl}+ \keystroke{B} & Mostrar Ficha Indivídual\\
        
        \keystroke{Ctrl}+ \keystroke{Shift}+ \keystroke{D} & Abrir programa de Diabetes\\
        \keystroke{Ctrl}+ \keystroke{Shift}+ \keystroke{H} & Abrir programa de Hipertensão\\
        \keystroke{Ctrl}+ \keystroke{Shift}+ \keystroke{M} & Abrir programa de Planeamento Familiar\\
        \keystroke{Ctrl}+ \keystroke{Shift}+ \keystroke{G} & Abrir programa de Saúde Materna\\
        \keystroke{Ctrl}+ \keystroke{Shift}+ \keystroke{I} & Abrir programa de Saúde Infantil\\
        \keystroke{Ctrl}+ \keystroke{Shift}+ \keystroke{O} & Abrir programa de Rastreio Oncológico\\
        \hline
    \end{longtable}

    \subsection{Módulo PEM Auto}

    TODO: Introdução

    \subsubsection{Utilização}



    \subsection{Configuração Avançada}\label{config}
    
    Secção em construção.






    \newpage
    \section{Suporte}

    \subsection{FAQ}

    Secção em construção.

    \subsection{Outras questões}

    Quaisquer dúvidas podem ser enviadas por e-mail para joao.p.rodrigo@gmail.com.





    \newpage
    \section{Como contribuir para o projecto}

    \subsection{Não tenho medo de um editor de texto/Programar}

    O código desta \emph{suite} é open source e está disponível no gitlab em \\

    \texttt{https://gitlab.com/joao.p.rodrigo/clinicoplus}\\

    Pode contribuir directamente através do git ou enviar-me um e-mail para joao.p.rodrigo@gmail.com.

    \subsection{Prefiro contribuir de outra forma}

    Se usar o ClínicoPlus e gostar, \textbf{partilhe}!
    
    Envie-me um e-mail para joao.p.rodrigo@gmail.com. Saber que não sou o único a utilizar as minhas ferramentas pode fazer a diferença entre continuar a publicá-las ou não.\\
    
    Neste momento nenhum dos meus projectos na área da informática geram qualquer remuneração. Todos eles têm sido disponibilizados gratuitamente. Todos os custos com servidores e taxas para publicação nas \emph{App Stores} sairam do meu bolso. O tempo que passo a programar não é utilizado a ganhar dinheiro ou estar com a família e amigos.\\    
    
    Eventualmente, aceito donativos para manter os projectos com um saldo menos negativo.

\end{document}