# ClínicoPlus



## Licença

[Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Notas importantes

* Deve adicionar na pasta o executável do Autohotkey:

**AutoHotkeyU64.exe**, 
disponível em: [https://www.autohotkey.com/download/ahk.zip](https://www.autohotkey.com/download/ahk.zip)

* Para arrancar facilmente a **Suite**, basta abrir o ficheiro **clinico_plus.bat**.
* Os ficheiros .ahk devem ser guardados com a codificação windows 1252.

