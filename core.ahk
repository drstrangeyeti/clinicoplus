; ## Licen�a

; Attribution-NonCommercial-ShareAlike 4.0 International
; https://creativecommons.org/licenses/by-nc-sa/4.0/


#SingleInstance force

#Include lib\helpers.ahk
#Include lib\pem-auto.ahk

^F12::togglePemAuto()

#IfWinActive SCl.nico.*

#include lib\analises.ahk
#include lib\snippets.ahk

Esc::clickButton("assets\bt_sair.png")
^R::
if (clickButton("assets\bt_resultados.png") != true)
	clickButton("assets\bt_actualizar.png")
return
^P::
if (clickButton("assets\bt_pem.png") != true)
	if (clickButton("assets\bt_imprimir.png") != true)
		clickButton("assets\bt_imprimir2.png")
return
^A::clickButton("assets\bt_mcdt.png")
^M::clickButton("assets\bt_marcar.png")
^S::clickButton("assets\bt_guardar.png")
^1::clickSoap(positions.s)
^2::clickSoap(positions.o)
^3::clickSoap(positions.a)
^4::clickSoap(positions.p)
^0::clickSoap(positions.fi)
^B::clickPrograma(programas.FI)

^+D::clickPrograma(programas.DM)
^+H::clickPrograma(programas.HT)
^+M::clickPrograma(programas.PF)
^+G::clickPrograma(programas.SM)
^+I::clickPrograma(programas.SI)
^+O::clickPrograma(programas.RO)

;^+A::corrigirDist()


