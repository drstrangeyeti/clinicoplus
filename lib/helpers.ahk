; ## Licen�a

; Attribution-NonCommercial-ShareAlike 4.0 International
; https://creativecommons.org/licenses/by-nc-sa/4.0/


SetTitleMatchMode RegEx
SendMode Input


; Diferen�a do botao resultado para o canto superior esquerdo
cornerX := -106
cornerY := -28

; Posi�oes relativas ao canto superior esquerdo dos botoes
; S 237
; O 373
; A 510
; P 649
; FI 154
; PF 290
; SM 345
; DM 400
; HT 460
; RO 515
; SI 570

positions := {s: [40, 237], o: [40, 373], a: [40, 510], p: [40, 649], fi: [815,77]}

programas := {FI: 154, PF: 290, SM: 345, DM: 400, HT: 460, RO: 515, SI: 570}

watchPixel(pixelX, pixelY, pixelColor, timeout := 4)
{
	miliTimeout := timeout * 5
	Loop, %miliTimeout%
	{
		PixelGetColor, color, %pixelX%, %pixelY%
		if (color = pixelColor)
		{
			return 1
		}
		sleep, 200
	}
	msgbox %color%
	return 0
}

sendAnalises(analises)
{
	for k,v in analises
	{
		sendInput %v%{enter}
		sendInput {enter}
		sleep, 500
	}
}

clickButton(button)
{
	MouseGetPos, OriginalX, OriginalY
	MouseMove, 0, 0, 0
	ImageSearch, FoundX, FoundY, 0, 0, A_ScreenWidth, A_ScreenHeight, %button%
	if ErrorLevel = 0 
	{
		MouseMove %FoundX%, %FoundY%, 0
	    Click %FoundX%, %FoundY%
	}
	MouseMove, %OriginalX%, %OriginalY%, 0
	if ErrorLevel = 0
		return true
	return false
}

clickSoap(soap)
{
	global cornerX, cornerY
	MouseGetPos, OriginalX, OriginalY
	MouseMove, 0, 0, 0
	ImageSearch, foundX, foundY, 0, 0, A_ScreenWidth, A_ScreenHeight, assets\bt_resultados.png
	if ErrorLevel = 0 
	{
		fX := foundX + cornerX + soap[1]
		fY := foundY + cornerY + soap[2]
		MouseMove, %fX%, %fY%, 0
		Click %fX%, %fY%
	}
	MouseMove, %OriginalX%, %OriginalY%, 0
}

clickPrograma(programa)
{
	global cornerX, cornerY, positions
	MouseGetPos, OriginalX, OriginalY
	MouseMove, 0, 0, 0
	ImageSearch, foundX, foundY, 0, 0, A_ScreenWidth, A_ScreenHeight, assets\bt_resultados.png
	if ErrorLevel = 0 
	{
		fY := foundY + cornerY + positions.o[2]
		fX :=  positions.o[1]

		PixelGetColor, color, %fX%, %fY%
		if (color != 0xCCF7F7)
		{
			MouseMove, %fX%, %fY%, 0
			Click %fX%, %fY%
		}
		if (watchPixel(fX, fY, 0xCCF7F7) = 1)
		{
			posY := programa 
			if (posY > 0)
			{
				posY := foundY + cornerY + posY
				MouseMove, 750, %posY%, 0
				click, 750, %posY%
			}
		}
		else
			MsgBox Failed
	}
	MouseMove, %OriginalX%, %OriginalY%, 0
}

corrigirDist()
{
	MouseGetPos, OriginalX, OriginalY
	MouseMove, 0, 0, 0
	ImageSearch, x2, y2, 0, 0, A_ScreenWidth, A_ScreenHeight, assets\bt_resultados.png
	if ErrorLevel = 0 
	{
		ImageSearch, x1, y1, 0, 0, A_ScreenWidth, A_ScreenHeight, assets\corner.png
		x3:= x2 - x1
		y3:= y2 - y1
		msgbox, x is %x3% y is %y3% 
	}

	MouseMove, %OriginalX%, %OriginalY%, 0
}
