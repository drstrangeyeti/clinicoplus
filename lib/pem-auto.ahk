; ## Licen�a

; Attribution-NonCommercial-ShareAlike 4.0 International
; https://creativecommons.org/licenses/by-nc-sa/4.0/


pemAuto := false
pemPass := ""


togglePemAuto()
{
	global pemAuto, pemPass
	pemAuto := !pemAuto
	if (pemAuto == true)
	{
		showTooltip("PEM auto ligado")
		SetTimer, procurarJanelaPassword, 500
		;InputBox, pemPass, Login Autom�tico PEM, Definir palavra-passe a introduzir na PEM:, HIDE
	} else {
		SetTimer, procurarJanelaPassword, Off
		pemPass := ""
		showTooltip("PEM auto desligado")
	}
}

procurarJanelaPassword()
{
	global pemAuto, pemPass
	if (pemAuto && WinExist("ahk_class SunAwtDialog"))
	{
		if (StrLen(pemPass) == 0)
		{
			InputBox, pemPass, PEM Auto, Defina palavra-passe a introduzir na PEM:, HIDE, , ,0,0
			; Apaga a password ao fim de 2h por motivos de seguran�a
			SetTimer, erasePemPass, -7200000
		}
		WinActivate, ahk_class SunAwtDialog
		WinWaitActive, ahk_class SunAwtDialog
		WinGetPos, CoordX, CoordY, largura, comprimento, ahk_class SunAwtDialog
		Loop, 200 {
			; 200*100 = 20 segundos, ap�s os quais sai do loop (poupa CPU se houver algum problema)
			ImageSearch, foundX, foundY, 0, 0, largura, comprimento, assets/lbl_pin_errado.png
			If (ErrorLevel = 0){
				pemPass := ""
				ErrorLevel := 1
				Break
			}
			ImageSearch, foundX, foundY, 0, 0, largura, comprimento, assets/input_pem.png
			If (ErrorLevel = 0) 
				Break
			sleep, 100
		}
		If (ErrorLevel = 0) {
			; Clica no input do Dialog da PEM, caso contr�rio � poss�vel n�o escrever no sitio certo
			xOffset := foundX + 20
			yOffset := foundY + 15
			click, %xOffset%, %yOffset%
			sleep, 100
			sendInput %pemPass%{Enter}
		}
	}
}

erasePemPass()
{
	global pemPass
	pemPass := ""
}

showTooltip(tooltipText)
{
	ToolTip, %tooltipText%
	SetTimer, RemoveToolTip, -3000
}

RemoveToolTip:
	ToolTip
return