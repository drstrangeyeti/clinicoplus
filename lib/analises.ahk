; ## Licen�a

; Attribution-NonCommercial-ShareAlike 4.0 International
; https://creativecommons.org/licenses/by-nc-sa/4.0/


::>ajuda::
MsgBox,  , Comandos Suportados, 
(
>basico: perfil basico de analises`n
>alargado: perfil alargado de analises`n
>lipidos: CT HDL Trigliceridos e CK`n
>hta: ECG e analises HTA`n
>htasec: despiste hta secundaria`n
>anemia: hemograma cinetica do ferro B12 folatos`n
>diabetes: perfil anual DM
)	
return

::>basico::
SendAnalises(["HEMOGRAMA COM PLAQUETAS", "GLUCOSE, DOSEAMENTO", "IONOGRAMA (NA, K, CL), S/U", "TSH", 338, "AST", "ALT", "GAMAGLUTAMIL TRANSFERASE", "FOSFATASE ALCALINA", 1029, 412, 620, 625, "CREATININA, S/U", 627])
return

::>alargado::
SendAnalises([1313, 1318, "PROTE%NAS (TOTAL) E ELECTROFORESE, S", "IONOGRAMA (NA, K, CL), S/U", 390, 499, "URINA%EXAME DIRECTO, CULTURAL, IDENTIFICA%O E TSA (UROCULTURA)", 1314, 1017, 1302, "TEMPO DE PROTROMBINA (TP, QUICK, INR)"])
return

::>lipidos::
SendAnalises(["CREATINAQUINASE (CK),", 1029, 412, 620])
return

::>hta::
SendAnalises(["ECG", "HEMOGRAMA COM PLAQUETAS", "GLUCOSE", 338, 1318, 1029, 412, 620, "IONOGRAMA (NA, K, CL), S/U", 625, "CREATININA, S/U", 627])
return

::>htasec::
SendAnalises([1053, "(FT4)", "(VMA)", 690, 1009, 1010, 663, 1032, 1315, "IONOGRAMA (NA, K, CL), S/U", "renina"])
return

::>anemia::
SendAnalises(["HEMOGRAMA COM PLAQUETAS", 484, 483, "FERRO, CAPACIDADE DE ", 329, "(CIANOCOBALAMINA)"])
return

::>diabetes::
SendAnalises(["HEMOGRAMA COM PLAQUETAS", "HGB A1C", "GLUCOSE, DOSEAMENTO", 1318, 1029, 412, 620, "CREATININA, S/U", 627, "(urocultura)"])
return


