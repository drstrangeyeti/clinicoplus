; ## Licen�a

; Attribution-NonCommercial-ShareAlike 4.0 International
; https://creativecommons.org/licenses/by-nc-sa/4.0/


::hta.s::Vem para consulta de manuten��o da sa�de - HTA.
::hta.p::
(
Pe�o controlo anal�tico, inclu�ndo microalbumin�ria e perfil lip�dico.
Pe�o registo de TA em ambulat�rio.
Dou recomenda��es de dieta e exerc�cio f�sico.
)

::dm.s::Vem para consulta de manuten��o da sa�de - DM.
::dm.p::
(
Pe�o controlo anal�tico, inclu�ndo perfil lip�dico, microalbumin�ria.
Pe�o HbA1c.
Dou recomenda��es de dieta e exerc�cio f�sico.
)

::is.p::
(
Recomenda��es gerais.
Explico sinais de alarme que devem motivar reobserva��o.
)

::si.o::
(
Boa vitalidade.
N�o palpo adenopatias.
Orofaringe: sem altera��es
Otoscopia:
  OD: sem altera��es
  OE: sem altera��es
AC: s1+s2, n�o ausculto sopros
AP: MV mantido, n�o ausculto RA
Abd�men:
  mole, depress�vel, indolor � palpa��o. 
  N�o palpo massas ou organomegalias. 
  Sem altera��es � percuss�o RHA+
Sem edemas perif�ricos.
)

::sa.o::
(
Bom estado geral.
Pele e mucosas coradas e hidratadas.
Eupneico
AC: S1+S2, r�tmicos, n�o ausculto sopros ou extrassons
AP: MV mantido, sem RA
Sem edemas perif�ricos
)

::abdn::
(
Abd�men mole, depress�vel, indolor � palpa��o, n�o palpo massas ou organomegalias. Sem altera��es � percuss�o. RHA+
)

::acpn::
(
AC: S1+S2, r�tmicos, n�o ausculto sopros ou extrassons
AP: MV mantido, sem RA
)

::.a98::Vem para consulta de manuten��o da sa�de

::.a45::Recomenda��es gerais.
::.beg::Bom estado geral.
::.a60::
::exames.s::
Send Vem mostrar exames pedidos na consulta anterior.{Enter}{Enter}A60
Return

::altera��se::
::altera�ose::
::altera�oes::
Send altera��es
Return

::renova�ao::renova��o